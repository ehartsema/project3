#!/usr/bin/python3

import site
site.addsitedir("./modules")

import sys
import numpy as np
import matplotlib.pyplot as plt

import setup
import makeplot

pr = 0.5
pb = 0.5
if len(sys.argv) == 2:
    width = int(sys.argv[1])
    height = width
elif len(sys.argv) > 2:
    width = int(sys.argv[1])
    height = int(sys.argv[2])
    if len(sys.argv) == 3:
        pr = 0.5
        pb = 0.5
    elif len(sys.argv) == 4:
        pr = float(sys.argv[3])
        pb = pr
    else:
        pr = float(sys.argv[3])
        pb = float(sys.argv[4])
else:
    print("use: ", sys.argv[0], "width [height] [pright] [pbottom]")
    exit()

printWidth = 2 if width <= 50 else 1
maze = setup.eller(width, height, pr, pb, True, printWidth)

