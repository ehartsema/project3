#!/usr/bin/python3

import site
site.addsitedir("./modules")

import numpy as np
import matplotlib.pyplot as plt

import setup
import makeplot

width = 5
height = 5

maze = setup.eller(50,25,0.5,0.5, True,2)
p,psource,psink = setup.maze_pres_init(maze)
#new_maze = setup.enlarge_maze(maze, 5)

p = 1.0*(p-np.min(p))
p[maze==1] = np.nan

print(p)

fig = makeplot.maze(p)
plt.show()

