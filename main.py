#!/usr/bin/python3
import site
site.addsitedir('./modules')

#import libraries
import numpy as np
import PyQt5
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from tqdm import tqdm

import makeplot
import calc
import setup
from math import sqrt

#maze parameters
width = 1 #width in blocks of pathwidth, Set width on 1 for channel flow and correct flowslice
height = 8 #height in blocks of pathwidth
pathwidth = 6 #with of the blocks

#physics settings
pres_start = 1 #starting pressure in pascal
pres_end = 0
dpres = pres_end-pres_start
L = 0.01 # real space spacing between pixels in meters
density = 10000
kin_visco = 1*10**(0)
bulk_visco = 1*10**(0)

#Calculate non-dimensional numbers
G = -dpres/(L*pathwidth*height)
vel0 = G*(L*pathwidth)**2/(8*kin_visco) # charecteristic velocity = half max velocity of plane poiseuille flow

Euler = (-dpres)/(density*vel0**2)
reynolds = density*vel0*L*pathwidth/kin_visco
reynolds_bulk = density*vel0*L*pathwidth/bulk_visco
print('vel0:',vel0)
print('Euler:',Euler,'Reynolds:', reynolds,'Reynolds_bulk:',reynolds_bulk)
print('t0:',L/vel0)

#simulation control parameters
niter = 1000      #number of iterations
dt = 0.001       #time step size
h = 1           #spatial step size

#initialize geometry and boundary conditions
print("generating maze...")
maze = setup.eller(width, height, pr=0.5, pb=0.5, verbose=False, print_width=3)


# initialize pressure field
pres = setup.maze_pres_init(maze)
pres *= pres_start

#generate finer mesh from starting conditions
print("refining mesh...")
maze = setup.enlarge_maze(maze, pathwidth)
pres = setup.enlarge_maze(pres, pathwidth)

#initialize velocity field
vel = setup.vel_init(maze.shape[0], maze.shape[1])

#add source and sink in maze (start and finish)
vel,pres = calc.apply_BC(vel,pres,pathwidth,pres_start,pres_end)



#solve Navier Stokes equations
print('computing navier stokes...')
for i in tqdm(range(niter)):
    dvel = calc.Navier_Stokes(Euler,reynolds,reynolds_bulk,pres,vel,dt,h,maze)
    vel += dvel
    #update pressure field
    pres = calc.update_pres(pres,vel,maze,h,dt,-dpres)
    vel,pres = calc.apply_BC(vel,pres,pathwidth,pres_start,pres_end)

#renormalize from non dimensionan numbers
vel *= vel0
#plot results
makeplot.flow(vel,pres,L)
makeplot.flowslice(vel,G/kin_visco, pathwidth, L, excact = True)
makeplot.maze(pres)
print(np.amax(vel[:,:,0]**2+vel[:,:,1]**2))