import site
site.addsitedir('./modules')

import numpy as np
from math import floor, sinh

import matplotlib.pyplot as plt
import matplotlib.animation as anim
plt.rcParams.update({'font.size': 16})

def flow(vel,pressure,L, filename='plots/flow.png'):
    """
    Parameters
    ----------

    filename : filename to store image, default = plots/magnetization.png
    
    Returns
    -------
    fig : matplotlib figure
    """
    
    fig0=plt.figure()
    #pressure = vel[:,:,0]**2+vel[:,:,1]**2
    plt.imshow(pressure)
    [width, height, *_] = vel.shape
    X = np.linspace(0,height-1,height)
    Y = np.linspace(0, width-1, width)
    U = vel[:,:,0]
    V = vel[:,:,1]
    plt.quiver(X,Y,U,V,angles='xy', scale_units='xy')#, scale=10)
    
    plt.xlabel('x')
    plt.ylabel('y')
    plt.tight_layout()
    fig0.savefig(filename)
    return fig0

def flowslice(vel, A, pw, L, excact = False, filename='plots/flowslice.png'):
    """
    Parameters
    ----------

    filename : filename to store image, default = plots/magnetization.png
    
    Returns
    -------
    fig : matplotlib figure
    """
    [width, height, *_] = vel.shape
    unit_conversion = 1000
    fig0=plt.figure()
    
    #magvel = np.sqrt(vel[round(width/2),:,0]**2+vel[round(width/2),:,1]**2)
    vel_aver = np.average(vel[:,:,:], axis=0)
    vel_var = np.var(vel[:,:,:], axis=0)
    error = unit_conversion*0.4*np.sqrt(np.sqrt(vel_var[:,0]**2+vel_var[:,1]**2))
    magvel = unit_conversion*np.sqrt(vel_aver[:,0]**2+vel_aver[:,1]**2)
    x = np.arange(0,height*L,L)
    plt.errorbar(x,magvel,error,ecolor='g',elinewidth=0.5,label = 'simulation result')
    
    if excact:
        y = np.linspace(height/3-1,height*2/3)*L
        y2 = np.linspace(0,height*1/3)*L
        plt.plot(y,unit_conversion*A/2*y2*((height*L/3)-y2),color='orange',label = 'analytic solution')
    plt.xlabel('x [m]')
    plt.ylabel('v [mm/s]')
    plt.tight_layout()
    plt.legend(loc=1, prop={'size': 12})
    fig0.savefig(filename)
        
    return fig0

def maze(maze, fig=None, filename='plots/maze.png'):
    """Returns a 2d image of a maze layout
    Parameters
    ----------
    maze : (<int>, <int>) boolean numpy array
    fig : matplotlib figure container

    Returns
    -------
    fig : matplotlib figure container
    """
    if fig is None:
        fig = plt.subplots()

    fig0 = plt.imshow(maze)
    fig0.figure.savefig(filename)
    
    return fig0






