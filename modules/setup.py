import site
site.addsitedir('./modules')

import numpy as np

def pres_init(maze,pres_start,pres_end):
    """
    Parameters
    ----------
    N: maze size is NxN
    -------
    pres_init: inital pressure (NxN)
    """
    # currently only works for pipe
    N = maze.shape[0]
    presx = np.linspace(pres_start,pres_end,N)
    pres =  maze*-1 +1
    for i in range(N):
        pres[i,:] = presx*pres[i,:]
        
    return pres

def vel_init(width, height=None):
    if height is None:
        height = width

    vel = np.zeros([width,height,2])
    return vel

def maze(N,width):
    """
    Parameters
    ----------
    N: maze size is NxN
    -------
    maze
    """
    begincoordinate = [round(N/2)-round(width/2),0]
    endcoordinate = [round(N/2)+round(width/2),N]
    maze = np.ones((N,N), dtype=bool)
    maze[begincoordinate[0]:endcoordinate[0],begincoordinate[1]:endcoordinate[1]] = 0
    maze[:,0] = 1
    maze[:,-1] = 1
    maze[maze==1]=np.nan
    return maze

def maze_pres_init(maze, source = None, sink = None):
    [w, h] = maze.shape
    maze = 1.0*maze #make sure maze is not a boolean or integer array
    maze[maze==1] = np.nan
    if source == None:
        source = [1, 1]
    
    if sink == None:  
        sink = [w-2, h-2]
    
    psource = map_pres(maze, source, sink) 
    psink = map_pres(maze, sink, source)
    p = psource - psink
    
    #convert p matrix to standard form
    p = 1.0*p
    p = p-np.min(p)
    p = p/np.max(p)
    p[np.isnan(maze)] = np.nan
    
    return p

def map_pres(maze, source, sink):
    p = np.zeros(maze.shape,dtype=int)
    p[source[0], source[1]] = 1
    while np.any(p==1):#p[sink[0], sink[1]]==0:
        mask = p + np.roll(p, 1, 0) + np.roll(p, 1, 1) \
                + np.roll(p, -1, 0) + np.roll(p, -1, 1)
        mask[np.isnan(maze)] = 0
        mask[mask != 0] = 1
        p = p + mask
        #print(p) #DEBUG
        #input("press any key") #DEBUG
    
    p[p!=0] = p[p!=0]-1
    return p

def eller(width, height, pr=0.5, pb=0.5, verbose=False, print_width=2):
    """Generates a two dimensional maze using Eller's Algorithm.
    
    Generate a Maze layout row per row using Eller's Algorithm.
    The maze does not have any loops, and is surrounded by solid walls.
    Every coordinate [x,y] = [2*<int>+1, 2*<int>+1] is
    guaranteed to be a passage (not a wall).
    
    Parameters
    ----------
    width: number of vertices in horizontal direction
    height: number of vertices in vertical direction
    pr: chance of creating a wall on the right side of a vertex
    pb: chance of creating a wall on the bottom of a vertex
    verbose : boolean, prints maze layout if true
    
    Returns
    -------
    maze : (2*height+1, 2*width+1) boolean numpy array containing a maze.
    
    """
    #generate empty maze
    maze = np.zeros([2*height+1, 2*width+1],dtype=bool)
    maze[:,0] = maze[-1,:] = 1
    maze[0,:] = maze[:,-1] = 1
    maze[::2,::2] = 1
    
    #allocate helper variables
    level = list(range(width))
    passages = np.empty(width, dtype=bool)

    for h_ in range(height):
        #add row walls
        for w_ in range(width-1):
            #Add walls between vertices in row.
            #If vertices are members of the same set, add them to prevent loops
            rr = np.random.rand()
            if pr > rr or level[w_] == level[w_+1]:
                maze[2*h_+1,2*w_+2] = 1
            else:
                level = [level[w_] if x == level[w_+1] else x for x in level]
                
        #for last layer of the maze, prevent creation of bottom passages in outer wall
        if h_ == height-1:
            break
        
        #Add bottom walls
        seen = set()
        #Iterate through each vertex.
        #If the vertex is from a newly seen path, make sure it has 
        #at least one passage to the next layer of the maze.
        for w_, a in enumerate(level):
            if not a in seen:
                seen.add(a)
                ids = list()
                for index in range(w_,width):
                    if level[index] == a:
                        ids.append(index)
                
                if len(ids) == 1:
                    sub_passages = np.array([True])
                elif len(ids) == 2:
                    sub_passages = np.array([True, False]) ^ (np.random.rand() > 0.5)
                else:
                    disconnected = True
                    while disconnected:
                        sub_passages = np.random.rand(len(ids)) > pb
                        if np.any(sub_passages):
                            disconnected = False
                
                passages[ids] = sub_passages
        
        #update maze matrix, prevent outer walls from turning into passages
        maze[2*h_+2,1:-1:2] = ~passages | maze[2*h_+2,1:-1:2]
        #generate new set list
        for w_, passage in enumerate(passages):
            if not(passage):
                level[w_] = max(level)+1

    #make sure all paths are connected 
    for w_ in range(width-1):
        if not(level[w_] == level[w_+1]):
            maze[-2,2*w_+2] = 0
            level = [level[w_] if x == level[w_+1] else x for x in level]
    
    #print maze layout if verbose
    if verbose:
        for h_, row in enumerate(maze):
            v = ""
            for block in row: 
                if block:
                    v = v + print_width*(chr(0x2588))
                else:
                    v = v + print_width*(chr(0x20))
            
            print(v)
    
    #convert to standard matrix
    maze = maze+0.0
    maze[maze==1]=np.nan
    return maze

def enlarge_maze(maze, multiplier):
    """Enlarges a maze by an integer multiplier.
    Parameters
    ----------
    maze : (<int>,<int>) numpy array
    multiplier : integer number of vertices to scale one vertex to
    
    Returns
    -------
    new_maze : rescaled maze
    
    """
    [width, height] = maze.shape
    new_maze = np.empty([multiplier*width, multiplier*height])
    for a in range(multiplier):
        for b in range(multiplier):
            new_maze[a::multiplier,b::multiplier] = maze
    
    #convert to standard matrix
    maze = maze+0.0
    maze[maze==1]=np.nan

    return new_maze

def maze_pres_init(maze, source = None, sink = None):
    [w, h] = maze.shape
    maze = 1.0*maze #make sure maze is not a boolean or integer array
    maze[maze==1] = np.nan
    if source == None:
        source = [1, 1]
    
    if sink == None:  
        sink = [w-2, h-2]
    
    psource = map_pres(maze, source, sink) 
    psink = map_pres(maze, sink, source)
    p = psource - psink
    
    #convert p matrix to standard form
    p = 1.0*p
    p = p-np.min(p)
    p = p/np.max(p)
    p[np.isnan(maze)] = np.nan
    
    return p

def map_pres(maze, source, sink):
    p = np.zeros(maze.shape,dtype=int)
    p[source[0], source[1]] = 1
    while np.any(p==1):#p[sink[0], sink[1]]==0:
        mask = p + np.roll(p, 1, 0) + np.roll(p, 1, 1) \
                + np.roll(p, -1, 0) + np.roll(p, -1, 1)
        mask[np.isnan(maze)] = 0
        mask[mask != 0] = 1
        p = p + mask
        #print(p) #DEBUG
        #input("press any key") #DEBUG
    
    p[p!=0] = p[p!=0]-1
    return p
