import site
site.addsitedir('./modules')
from tqdm import tqdm
import numpy as np

def Navier_Stokes(Euler,reynolds,reynolds_bulk,pres,vel,dt,h,maze):
    """ Numerically solve Navier Stokes equation for incompressible flow
    Parameters
    ----------
    density: fluid density
    kin_visco: kinematic viscosity
    pres: (N,N) pressure
    vel: (N,N,2) velocity at all point in the maze, for both x and y direction
    dt: time step
    h: spatial stepsize (dx)
    index: only loops over internal maze points, 
    no boundaries as velocity is alwasy 0 there (no slip BC)
    
    -------
    new velocity for index 
    """
    dvel = np.zeros(vel.shape)
    result = np.where(maze == 0)
    coord_list= list(zip(result[0], result[1]))
    
    for index in coord_list:
        #solve directional derivative gradient
        grad_v = vel_grad(vel,index,h)
        term1 = -np.dot(vel[index[0],index[1],:],grad_v)
        
        # solve pressure gradient
        grad_p = press_grad(pres,index,h)
        term2 = -Euler*grad_p
    
        # solve velocity laplacian
        lap_v = vel_laplace(vel,index,h)
        term3 = 1/reynolds*lap_v
        
        # solve velocity divergence
        grad_div = grad_div_vel(vel,index,h)
        term4 = (1/reynolds+1/reynolds_bulk)*grad_div
        
        # euler forwards
        dvel[index] = (term1 + term2 + term3 + term4)*dt
    return dvel

def vel_grad(vel,index,h):# definition see: https://en.wikipedia.org/wiki/Material_derivative, all the way at the bottom
    Grad = np.zeros([2,2])
    for i in range(2):
        for j in range(2):
            x0 = vel[index[0]-(j),index[1]-(1-j),i]
            x2 = vel[index[0]+(j),index[1]+(1-j),i]
            Grad[j,i] = midpoint(x0,x2,h)
    return Grad

def press_grad(press,index,h):
    Grad = np.zeros(2)
    for j in range(2):
        x0 = press[index[0]-(j),index[1]-(1-j)]
        x2 = press[index[0]+(j),index[1]+(1-j)]
        Grad[j] = midpoint(x0,x2,h)
        
    Grad[np.isnan(Grad)]=0
    return Grad

def vel_laplace(vel,index,h):
    Lap = np.zeros(2)
    for i in range(2):
        for j in range(2):
            x0 = vel[index[0]-(j),index[1]-(1-j),i]
            x1 = vel[index[0],    index[1],    i]
            x2 = vel[index[0]+(j),index[1]+(1-j),i]
            Lap[i] += second_central(x0,x1,x2,h)
    return Lap

def vel_div(vel,index,h):
    Div = 0
    for i in range(2):
        x0 = vel[index[0]-i,index[1]-(1-i),i]
        x2 = vel[index[0]+i,index[1]+(1+i),i]
        Div += midpoint(x0,x2,h)
    return Div

def grad_div_vel(vel,index,h):
    Grad = np.zeros(2)
    for i in range(2):
        x0 = vel_div(vel,[index[0]-(i),index[1]-(1-i)],h)
        x2 = vel_div(vel,[index[0]+(i),index[1]+(1-i)],h)
        Grad[i] = midpoint(x0,x2,h)
    return Grad

def midpoint(x0,x2,h):
    return (x2-x0)/(2*h)

def second_central(x0,x1,x2,h):
    return (x0-2*x1+x2)/(h**2)


def apply_BC(vel,pres,pw,pres_start,pres_end):
    #add sources
    pres[pw,pw:2*pw]=pres_start
    pres[-pw-1,-2*pw:-pw]=pres_end
    
    #vel[pw-1,pw:2*pw,1]=1
    #vel[-pw,-2*pw:-pw,1]=1
    return (vel, pres)

def update_pres(pres,vel,maze,dx,dt,dp0):
    
    result = np.where(maze == 0)
    coord_list= list(zip(result[0], result[1]))
    #loop over all indecies
    for index in coord_list:
        # x direction
        xflow = vel[index[0],index[1],0]*dx
        sgn = int(np.sign(xflow))
        if maze[index[0],index[1]+sgn] != np.nan:
            dp = np.abs(xflow)*dt/(dx**2)/dp0
            pres[index] -= dp
            pres[index[0],index[1]+sgn] += dp
            
        # y direction
        yflow = vel[index[0],index[1],1]*dx
        sgn = int(np.sign(yflow))
        if maze[index[0]+sgn,index[1]] != np.nan:
            dp = np.abs(yflow)*dt/(dx**2)/dp0
            pres[index] -= dp
            pres[index[0]+sgn,index[1]] += dp
    return pres
